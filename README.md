# Assignment

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 9.0.5.

# Steps to follow for running the project locally.

1. clone the git repository using  the below command
   git clone https://SumanBalaji@bitbucket.org/SumanBalaji/himpact-assignemnt.git.
2. cd himpact-assignment
3. npm i
4. npm start or ng serve

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

