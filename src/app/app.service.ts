import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map, catchError, tap } from 'rxjs/operators';
import { NgxSpinnerService } from "ngx-spinner";

@Injectable({
  providedIn: 'root'
})
export class AppService {
  apiURL: string = 'https://reqres.in/api/users';
  constructor(private httpClient: HttpClient, private spinner: NgxSpinnerService) { }
  public getUsers(url?: string) {
    return this.httpClient.get<any>(`${this.apiURL}?page=${url}`,
      { observe: 'response' }).pipe(tap(res => {
        console.log(res);
        return res;
      }));
  }
  public getUserById(id?: string) {
    return this.httpClient.get<any>(`${this.apiURL}/${id}`,
      { observe: 'response' }).pipe(tap(res => {
        console.log(res);
        return res;
      }));
  }
  enableLoader() {
    this.spinner.show();
  }
  disableLoader() {
    this.spinner.hide();
  }
}
