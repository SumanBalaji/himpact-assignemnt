import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AppService } from '../app.service';
import { User } from './dropdown';
@Component({
  selector: 'app-dropdown',
  templateUrl: './dropdown.component.html',
  styleUrls: ['./dropdown.component.css']
})
export class DropdownComponent implements OnInit {
  usersList = [];
  count = 1;
  constructor(public router: Router, public appService: AppService) {
  }
  ngOnInit() {
    this.getAllUsers(this.count);
  }
  // function to navigate to userdetails page
  navigateToUserPage(id) {
    this.router.navigate([`user/${id}`]);
  }
  // function to get all the users
  getAllUsers(id?) {
    this.appService.enableLoader();
    this.appService.getUsers(id).subscribe(res => {
      res.body.data.map(ele => this.usersList = [...this.usersList, new User(ele)]);
      this.usersList.sort((a, b) => a.first_name > b.first_name ? 1 : -1);
      this.appService.disableLoader();
      this.count += this.count;
      if (this.count > res.body.total_pages)
        return;
      this.getAllUsers(this.count);
    });
  }
}

