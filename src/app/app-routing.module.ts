import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UserComponent } from './user/user.component';
import { DropdownComponent } from './dropdown/dropdown.component';


const routes: Routes = [
  { path: '', component: DropdownComponent },
  { path: 'user/:id', component: UserComponent }];
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
