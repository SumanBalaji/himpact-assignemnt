import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { AppService } from '../app.service';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {
  public userId;
  public userDetail;
  constructor(public router: Router, public route: ActivatedRoute, public appService: AppService) { }
  ngOnInit(): void {
    //retrieving the user id passed through routing url
    this.route.params.subscribe(res => this.userId = res.id);
    this.getUserDetails();
  }
  // function to get the userdetail based on userid
  getUserDetails() {
    //to enable the loader
    this.appService.enableLoader();
    this.appService.getUserById(this.userId).subscribe(res => {
      console.log(res)
      this.userDetail = res.body['data'];
      //to disable the loader
      this.appService.disableLoader();
    });
  }
}
